## Situation Awearness Application With python and GeoDjango

![mapView](screenshots/mapview.png?raw=true "mapview")



![incidence query](screenshots/incidence_query.png?raw=true "incidence_query")



![hot_zone_query](screenshots/hot_zone_query.png?raw=true "hot_zone_query")



## About
This is a situation awareness application built on top of Django, a python web framework. This application implements 
Django's GIS capability known as GeoDjango and leaflet for the front end with different view scales ranging from:

- darkscale view
- grayscale view
- satellite view
- street view

In this application, incidence are updated from the admin interface and displayed on the map, indicated by a marker.
each event or incidence has a relative color of marker defined from the javascript used for the front end to diplay the 
leaflet map. Sames goes for the hot zones, visualized by circle markers of different colors defined by javasript support for the leaflet map.



## Technology and Requirements
1. Python 3
2. Django 2.2
3. Database that can be used:

- PostgreSQL (with PostGIS)

- MySQL (mostly with MyISAM engine)

- Oracle

- SQLite (with SpatiaLite)


4. leaflet Map



## Installations
In general, GeoDjango installation requires

1. [Python](https://www.python.org/downloads/) 

2. [Django](https://docs.djangoproject.com/en/2.2/topics/install/)

3. [Spatial Database (postgis)](https://docs.djangoproject.com/en/2.2/ref/contrib/gis/install/postgis/)

4. [Installing Geospatial libraries](https://docs.djangoproject.com/en/2.2/ref/contrib/gis/install/geolibs/)

5. [PostgreSql](http://www.techken.in/how-to/install-postgresql-10-windows-10-linux/)

6. [psycopg2](http://initd.org/psycopg/docs/install.html)

`(venv)path/to/situationAwearness/src$ pip install -r requirements.txt
`


## Setting Up
[setting up and geodjango tutorial](https://docs.djangoproject.com/en/2.2/ref/contrib/gis/tutorial/#setting-up)


## Run App
1. make sure your virtualenv is ativated.

`
(venv)path/to/situationAwearness/src$
`

2. make sure you are in the same directory where manage.py is then run

`(venv)path/to/situationAwearness/src$ python manage.py runserver
`

3. go to your web browser and enter localhost:8000 



## Resources
1. [Django2.2 Doc](https://docs.djangoproject.com/en/2.2/)
2. [GeoDjango Doc](https://docs.djangoproject.com/en/2.2/ref/contrib/gis/install/postgis/)
3. [leafletjs Website](https://leafletjs.com)


## Other Resources
1. [FrontEnd](https://startbootstrap.com/previews/sb-admin-2/)
2. [Wanjohi Kibui(video tutorial)](https://www.youtube.com/playlist?list=PL7amXK4vKqATa_KrfQ3_tEF_ywAgAqWeJ)