# Generated by Django 2.2.3 on 2019-09-04 10:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('incidence', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='incidence',
            name='action_taken',
            field=models.TextField(default='Detail of incident', max_length=500),
        ),
        migrations.AddField(
            model_name='incidence',
            name='detail_of_event',
            field=models.TextField(blank=True, max_length=1000, null=True),
        ),
        migrations.AddField(
            model_name='incidence',
            name='incident',
            field=models.CharField(choices=[('Select_Incidence', 'Select_Incidence'), ('IED', 'IED'), ('AMBUSH', 'AMBUSH'), ('SUICIDE BOMBIMG', 'SUICIDE BOMBIMG')], default='Select_Incidence', max_length=20),
        ),
        migrations.AddField(
            model_name='incidence',
            name='time_of_occurance',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AddField(
            model_name='incidence',
            name='updated',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
    ]
