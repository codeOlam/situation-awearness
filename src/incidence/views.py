from django.shortcuts import render
from django.views.generic import TemplateView, DetailView, ListView, View
from django.core.serializers import serialize
from django.http import HttpResponse
from django.db.models import Q
# Create your views here.

from .models import Incidence, Places, Hot_Zone

class IncidenceView(View):
	model = Incidence


	def get(self, request, *args, **kwargs):
		queryset 	= Incidence.objects.all()
		query 		= self.request.GET.get('q')
		template 	= 'incidence_stat.html'

		if query:
			incident = Incidence.objects.filter(
				Q(incident__icontains=query)|
				Q(incident__iexact=query)|
				Q(location__icontains=query)|
				Q(location__iexact=query)|
				Q(state__icontains=query)|
				Q(state__iexact=query)|
				Q(time_of_occurance__icontains=query)|
				Q(time_of_occurance__iexact=query)
			).distinct()

			context = {
				'incident': incident,
				'query': query
			}

			return render(request, template, context)

		else:
			context	= {
				'queryset': queryset
			}

			return render(request, template, context)


class HotZoneView(View):
	model 		= Hot_Zone

	def get(self, request, *args, **kwargs):
		hotzone_list		= Hot_Zone.objects.all()
		hotzone_query 		= self.request.GET.get('q')
		template 			= 'hotzone_stat.html'

		if hotzone_query:
			hotzones = Hot_Zone.objects.filter(
				Q(activity__icontains=hotzone_query)|
				Q(activity__iexact=hotzone_query)|
				Q(location__icontains=hotzone_query)|
				Q(location__iexact=hotzone_query)|
				Q(state__icontains=hotzone_query)|
				Q(state__iexact=hotzone_query)
			).distinct()

			context = {
				'hotzones': hotzones,
				'hotzone_query': hotzone_query
			}
			
			return render(request, template, context)

		else:
			context = {
				'hotzone_list': hotzone_list
			}

			return render(request, template, context)


def places_dataset(self):
	places 		= serialize('geojson', Places.objects.all())


	return HttpResponse(places, content_type='json')

def incidence_dataset(self):
	incidence 	= serialize('geojson',
							Incidence.objects.all())

	return HttpResponse(incidence, content_type='json')


def hot_zone_dataset(self):
	hotzone 	= serialize('geojson',
							Hot_Zone.objects.all())

	return HttpResponse(hotzone, content_type='json')



