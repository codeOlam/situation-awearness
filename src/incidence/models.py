from django.db import models
from django.contrib.gis.db import models
# Create your models here.

incident_list 	 = (('Select_Incidence', 'Select_Incidence'),
					('IED', 'IED'),
					('AMBUSH', 'AMBUSH'),
					('SUICIDE BOMBIMG', 'SUICIDE BOMBIMG') 
					)

activity_list	 = (('KIDDNAPPING', 'KIDDNAPPING'),
					('CATTLE RUSTLING', 'CATTLE RUSTLING'),
					('MILITANT', 'MILITANT'),
					('BANDICT', 'BANDICT'),
					('BOKO HARAM', 'BOKO HARAM'),
					('HERDS MEN', 'HERDS MEN')

	)


class Incidence(models.Model):
	"""
	Create models for incidence to be reported
	"""
	state				= models.CharField(max_length=150)
	incident 			= models.CharField(max_length=20, choices=incident_list, default="Select_Incidence", null=False)
	detail_of_event		= models.TextField(max_length=1000, null=True, blank=True)
	action_taken		= models.TextField(max_length=500, null=False, blank=False, default='The action taken was... ')
	time_of_occurance	= models.DateTimeField(auto_now=True, null=False, blank=False)
	updated				= models.DateTimeField(auto_now_add=True, null=True, blank=False)
	location			= models.PointField(srid=4326)



	#objects		= models.GeoManager()


	def __str__(self):
		return self.state



	class Meta:
		verbose_name 			= "Incidence"
		verbose_name_plural 	= "Incidences"


class Hot_Zone(models.Model):
	state			= models.CharField(max_length=150)
	activity 		= models.CharField(max_length=30, choices=activity_list)
	time_added		= models.DateTimeField(auto_now=True, null=False, blank=False)
	updated			= models.DateTimeField(auto_now_add=True, null=True, blank=False)
	location		= models.PointField(srid=4326)

	def __str__(self):
		return self.activity


	class Meta:
		verbose_name			= "Hot Zone"
		verbose_name_plural		= "Hot Zones"


class Places(models.Model):
    id_0 		= models.BigIntegerField()
    iso 		= models.CharField(max_length=3)
    name_0 		= models.CharField(max_length=75)
    id_1 		= models.BigIntegerField()
    name_1 		= models.CharField(max_length=75)
    type_1 		= models.CharField(max_length=50)
    engtype_1 	= models.CharField(max_length=50)
    nl_name_1 	= models.CharField(max_length=50)
    varname_1 	= models.CharField(max_length=150)
    geom 		= models.MultiPolygonField(srid=4326)


    def __str__(self):
    	return self.name_1


    class Meta:
    	verbose_name 			= "Place"
    	verbose_name_plural 	= "Places"