from django.contrib import admin
from django.contrib.gis import admin

from leaflet.admin import LeafletGeoAdmin
from .models import Incidence, Hot_Zone, Places
# Register your models here.

"""
#using OSMGeoAdmin

class IncidenceAdmin(admin.OSMGeoAdmin):
	list_display = ('state',
					'location')


admin.site.register(Incidence, IncidenceAdmin)
"""


class IncidenceAdmin(LeafletGeoAdmin):
	list_display	= ('state',
						'incident',
						'location',)

class HotZoneAdmin(LeafletGeoAdmin):
	list_display	= ('state',
						'activity',
						'location')

class PlacesAdmin(LeafletGeoAdmin):
	list_display	= ('name_1',
						'id_1',)



admin.site.register(Incidence, IncidenceAdmin)
admin.site.register(Hot_Zone, HotZoneAdmin)
admin.site.register(Places, PlacesAdmin)